import cv2 as cv
import numpy as np

#-------------------Funcion-------------------
def euclideo(img, angl,tx=0, ty=0): #img  -->imagen
                                    #angl -->Angulo
                                    #tx   -->Traslacion en x
                                    #ty   -->Traslacion en y
    (h, w) = img.shape[:2]          #alto y ancho de imagen
    angle = np.radians(angl)   #convencion antihorario, en radianes porque utiliza la matriz

    dst = np.float32([  [np.cos(angl),  np.sin(angl), tx],    #Matriz de transformacion euclideana
                        [-np.sin(angl), np.cos(angl), ty],])

    euclideo = cv.warpAffine(img, dst, (h, w))  
    return euclideo
#--------------------Programa------------------------
blue = (255 ,  0 ,  0 )
xi,yi,xf,yf= 0, 0, 0, 0
drawing = False

def select (event, x, y, flags, param):
    linea = 2 #ancho de linea 
    global xi,yi,xf,yf,drawing,img,width
    if event == cv.EVENT_LBUTTONDOWN:
        drawing = True
        xi, yi = x, y
    elif event == cv.EVENT_MOUSEMOVE:
        if drawing is True:
            img = act.copy()
            cv.rectangle(img,(xi, yi),(x,y),blue,linea)

    elif event == cv.EVENT_LBUTTONUP:
        drawing =False
        if x < 0: x = 0 
        if y < 0: y = 0
        xf, yf = x, y   



img = cv.imread('hoja.jpg', 1) 
xi, yi = 0, 0                      
xf, yf = img.shape[0], img.shape[1]    
act=img.copy()
baup=img.copy()

Angulo= int(input("Ingrese el angulo a rotar:\n"))
Xt= int(input("Ingrese traslacion en X:\n"))
Yt= int(input("Ingrese traslacion en Y:\n"))

cv.namedWindow("Image")
cv.setMouseCallback("Image",select)

while 1:
    cv.imshow('Image', img)
    opt = cv.waitKey(1) & 0xFF 
    if opt == ord('r'):
        img = baup.copy()
        act = baup.copy()
        xi,yi=0,0
        xf,yf= img.shape[0], img.shape[1]

    elif opt == ord('g'):
        xi,xf=min(xi,xf),max(xi,xf)
        yi,yf=min(yi,yf),max(yi,yf)
        act=baup[yi:yf, xi:xf]
        img=act.copy()
        cv.imwrite('recorte.png',act)

    elif opt == ord('e'):
        xi, xf = min(xi, xf), max(xi, xf)
        yi, yf = min(yi, yf), max(yi, yf)
        act = baup[yi:yf, xi:xf]
        act = euclideo(act,Angulo,Xt,Yt)
        img = act.copy()
        cv.imwrite('Resultado_Euclideo.png',act)
    
    elif opt == ord('q'):
        break