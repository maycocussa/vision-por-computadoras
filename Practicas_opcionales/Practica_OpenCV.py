import cv2
import numpy as np

#cv2.imread(nombre archivo, [Flags])

# Flags: cv2.IMREAD_COLOR -> abre imagen a color , puede se tambien 1
#        cv2.IMREAD_GRAYSCALE -> abre imagen en grises  puede ser tambien 0
#        cv2.IMREAD_UNCHANGED -> abre imagen con canal alpha

#cv2.imshow("nombre ventana", imagen.png)

#cv2.waitKey(0) -> se queda esperando una tecla

#cv2.destroyAllWindows() -> cierra todas la ventanas

#cv2.destroyWindow(nvent) -> destruye la ventana de nombre nvent

#cv2.imwrite(nombre archivo, img) -> escribe una imagen con nombre de archivo

# k = cv2.waitKey(0) ->guarda en k la tecla presionada en codigo ascii

# ord -> devuelve el codigo de  un caracter string

#---------------------------------VIDEO---------------------------------

# cv2.VideoCapture(device) -> device puede ser un string o un indice- las camaras aparecen como
# archivos /dev/video0

#isOpened -> si esta correctamente abierta
#read() -> capturar
#release() -> para liberar la camara 




def ejemplo1():
    print("Cargar la imagen en:\n\t\t\tEscala de grises  -> presione G \n\t\t\tEscala de colores -> presione C\n\t\t\tsalir -> cualquier tecla")
    
    k= str(input("\nIngrese la opcion: "))
    

    if k == 'g':
        img1=cv2.imread("hoja.jpg",0)
        cv2.imshow("Imagen en Gris",img1)
        cv2.waitKey(10000)
        cv2.destroyAllWindows()
    if k == 'c':
        img1=cv2.imread("hoja.jpg",1)
        cv2.imshow("Imagen en Color",img1)
        cv2.waitKey(10000)
        cv2.destroyAllWindows()
    else:
        print("Presiono para salir")
    

def video1():
    cap = cv2.VideoCapture (0) #abrimos camara 0
    while(True):
        ret , frame = cap.read() #devuelve una tupla,en ret(variable booleana) el estado, en frame devuelve la imagen
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY) #operamos sobre el cuadro de imagen
        cv2.imshow("Video en gris", gray)
        if((cv2.waitKey(1) & 0xFF) == ord('q')): #0XFF pone los 3 bist mas significativos en cero para acertar la lectura del ascii
            break
    cap.release()
    cv2.destroyAllwindows()






#---------------PROGRAMAS -> Descomentar para activar-----------------------------------
ejemplo1()
#video1()