#usamos funcion
#dts=cv2.warpAffine(scr,M,dzise[,dst])
#es una funcion generica que nos permite aplicar una matriz de transformacion M de tamaño 2x3
# scr --> es la imagen a transformar
# M --> es la matriz de transformacion
# dzise --> es el tamaño de la imagen de salida
# dst --> imagen de salida que le podemos pasar en forma opcional , en caso  contrario es el valor de retorno de la funcion
#------------Traslacion----------------
#         M = | 1 0 x |           -->  matriz de rotacion de 0 grados es la 2x2 identidad
#             | 0 1 y |           --> tercer columna valores de x e y de pixel que vamos a trasladar 
#-----------ejemplo--------------------
import numpy as np
import cv2
from matplotlib import pyplot as plt
import pylab as pl

def translate(image,x,y):
    (h,w) = (image.shape[0], image.shape[1])
    
    M= np.float32([[1,0,x],   #convertimos a nunpy arrary de float
                   [0,1,y]])

    shifted =cv2.warpAffine(image,M,(w,h))

    return shifted


img=cv2.imread("hoja.jpg",1)

dst=translate(img,x=100,y=100)
pl.figure("Traslación")
pl.axis("equal")
pl.axis("off")
plt.subplot(121), plt.imshow(img), plt.title("Input traslate") #subplot de una fila y dos columnas imagen 1 --> 121
plt.subplot(122), plt.imshow(dst), plt.title("Output traslate")
plt.show()

#------------Rotacion----------------
# M=cv2.getRotationMatrix2D(center,angle,scale) --> para calcular la matrix de rotacion
# center --> es el centro de rotacion de la imagen de entrada
# angle  --> es el anglulo de rotacion entendido en sentido antihorario
# scale --> factor de escala
# M es la matriz de rotacion
# cv2.warpAffine

#----------------Matriz de Rotacion Basica
#         M = | 1 0 x |           -->  matriz de rotacion de 0 grados es la 2x2 identidad
#             | 0 1 y |           --> tercer columna valores de x e y de pixel que vamos a trasladar 
#


#        Mr = |  s.cos(Ang)  s.sin(Ang)  [1-s.cos(Ang)]x-sin(Ang).y     | -->  matriz de rotacion de 0 grados es la 2x2 identidad
#             | -s.sin(Ang)  s.cos(Ang)  s.sin(Ang).x-(1-s.cos(Ang)).y  | --> tercer columna valores de x e y de pixel que vamos a trasladar 
#-----------ejemplo--------------------

# s     --> Es el factor de escala isotropico
# x e y --> Coordenadas del centro de rotacion

def rotate(img , angle, center=None, scale=1):
    (h,w)=img.shape[:2]

    if center is None:
        center=(w/2,h/2)  #defino el la mitad de la imagen como centro
    
    M = cv2.getRotationMatrix2D(center,angle,scale)
    
    rotate= cv2.warpAffine(img,M,(w,h))
    return rotate

dst2=rotate(img,angle=35,) #poner un centro distinto de la mitat ejmplo center=(0,0)
pl.figure("Rotación")
pl.axis("equal")
pl.axis("off")
plt.subplot(121), plt.imshow(img), plt.title("Input rotate") #subplot de una fila y dos columnas imagen 1 --> 121
plt.subplot(122), plt.imshow(dst2), plt.title("Output rotate")
plt.show()



#------------Flip(espejado)----------------
#usamos la funcion dst=cv2.flip(src,flipCode[,dst])
# src --> imagen a transformar
# flipCode es un numero entero que indica la forma del espejado
#  0 --> indica espejado sobre eje x
#  1 --> indica espejado sobre el eje y
# -1 --> indica espejado en x e y simultaneamente
# dst --> imagen de salida
#------------------------------

modes={ 'x':0,'y':1,'b':-1 } #creo diccionario  con los valores de x, y , b

def flip(img,mode):
    if mode not in modes.keys(): #chequea que el modo sea valido, si no retorma imagen
        return img
    flipped= cv2.flip(img,modes[mode])
    return flipped

dst3=flip(img,"y")

pl.figure("Espejado")
pl.axis("equal")
pl.axis("off")
plt.subplot(121), plt.imshow(img), plt.title("Input flip") #subplot de una fila y dos columnas imagen 1 --> 121
plt.subplot(122), plt.imshow(dst3), plt.title("Output flip")
plt.show()


