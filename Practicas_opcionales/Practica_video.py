import sys  #primer argumento nombre de archivo= Practico_03.py y el segundo argumento el archivo
import cv2  #de video= video.mp4 --> ejecutando --> python3 Practico_03.py video.mp4
if(len(sys.argv)>2):     
    filename = sys.argv[1]
    milisec = int(sys.argv[2]) #paso el string a entero
else:
    print("Pasar el nombre del video como argumento 1 y milisegundos como argumento 2")
    sys.exit(0)
cap = cv2.VideoCapture(filename)
while(cap.isOpened()):
    ret,frame=cap.read()
    gray=cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    cv2.imshow("Archivo de video",gray)
    if((cv2.waitKey(milisec) & 0XFF) == ord("q")): #espera los milisec para pasar otro frame
        break
cap.release()
cv2.destroyAllWindows()


# Para guardar video cv2.Writer([Filename,fourcc,fps,frameSize])
# filename  --> nombre del video
# fourcc    --> codificacion del video, DIVX,XVID,MJPG,X264,WMV1,WMV2
# fps       --> numero de cuadros por segundo
# frameZise --> tupla con la resolucion del video (W,H) -> (ancho,alto)