"""range(stop)
range(start,stop[,step])
start por defecto en cero
range(4) -->  produce 0,1,2,3
range(i,j) --> produce i ,i+1 ,i+2....j-1
range(4,0,-1) --> produce 4,3,2,1
RANGE DEVUELVE UN ITERADOR, NO UNA LISTA NI TUPLA"""


import cv2
#------------Estilo c, c++
"""img=cv2.imread("hoja.jpg",0)
h,w= img.shape
thr=200

for i in range(h):
    for j in range(w):
        if(img[i,j]>thr):
            img[i,j]=255
        else:
            img[i,j]=0

cv2.imwrite("resultadocosasdepython.jpg",img)"""

#-------------------------Estilo python

"""img =cv2.imread("hoja.jpg",0)
thr=200

for row in img:                   #primer elemento de imagen , una lista
    for col in row:               #devolve el primer elemento de la lista row
        print(col)                #imprimo por cmd el valor del pixel
        if(col>thr):              #los enteros y las tuplas son inmitables
            col=255               #cuando le cargo un numero diferente, apunta a otro lado por ser un entero
        else:
            col=0
cv2.imwrite("resultadocosasdepython2.jpg",img)"""

#----------------
#La sentencia id me permite determinar que valor tienen el elemnto que estoy utiliando como variable

""" img =cv2.imread("hoja.jpg",0)
thr=200

for row in img:
    for col in row:
        print("type(col) = ",type(col))
        print("id col = ", id(col))
        print("col = ", col)

        col=0
        micol=col
        
        print("col = ", col)
        print("id col = ", id(col))
        print("id micol", id(micol))
        break
    break """

#-------------usando nunpy

img=cv2.imread("hoja.jpg",0)
thr=200

img[img>=thr]=255  #asigno el valor 255 a todos los puntos de imagen y lo guardo en imagen
img[img<= thr]=0

cv2.imwrite("resultadocosasdepython3.jpg",img)

#--------------------- umbralizado con OpenCv----------------------------------
"""sive para umbralizado fijo a todos los elementos de un arrreglo"""
# retval,dist=cv2-threshold(src,thresh,maxval,type[,dts])
# src --> arreglo a umbraliar
# threshhold --> umbral a aplicar
# maxval --> valor a guardar en el arreglo en caso que sea mayor que el umbral
# type --> tipo de umbralizacion, por defector es binaria
#cv2.THRESH_BINARY
#cv2.THRESH_BINARY_INV
#cv2.THRESH_TRUNC
#cv2.THRESH_TOZERO
#cv2.THRESH_TOZERO_INV
# EXISTEN DOS UMBRALIZADOS QUE CALCULAN UMBRAL OPTIMO EN FORMA AUTOMATICA
#cv2.THRESH_OTSU
#cv2.THRESH_TRIANGLE

