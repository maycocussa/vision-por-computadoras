#EVENT MOUSEMOVE        --> El puntero del mouse se movio
#EVENT_LBUTTONDOWN      --> Se apreto el boton izquierdo
#EVENT_RBUTTONDOWN      --> Se apreto el boton derecho
#EVENT_MBUTTONDOWN      --> Se apreto el boton medio
#EVENT_LBUTTONUP        --> Se solto el boton izquierdo
#EVENT_RBUTTONUP        --> Se solto el boton derecho
#EVENT_MBUTTONUP        --> Se solto el boton medio
#EVENT_RBUTTONDBLCLK    --> Doble click boton izquierdo
#EVENT_RBUTTONDBLCLK    --> Doble click boton derecho
#EVENT_MBUTTONDBLCLK    --> Doble click boton medio
#EVENT_MOUSEWHEEL       --> Scroll vertical, positivo hacia adelante
#EVENT_MOUSEHWHEEL      --> Scroll Horizontal, positivo a la derecha

import cv2
import numpy as np

blue = (255 ,  0 ,  0 )
green = (0 ,  255 ,  0 )
red = (0 ,  0 ,  255)
xybutton_down= 0 ,0
# funcion de mouse callback
def draw_circle ( event ,  x ,  y ,  flags ,  param ) :
    global xybutton_down
    if event == cv2.EVENT_LBUTTONDOWN:
        print(" cv2.EVENT_LBUTTONDOWN",event)
        xybutton_down = x,y 
        cv2.circle(img ,  xybutton_down ,  9 ,  red ,  2)
    elif event == cv2.EVENT_RBUTTONDOWN:
        print(" cv2 .EVENT_RBUTTONDOWN",  event)
        cv2.circle(img ,(x,y),5,green,-1)
    elif event == cv2.EVENT_LBUTTONUP:
        print(" cv2 .EVENT_LBUTTONUP",event)
        cv2.line(img,xybutton_down,(x,y),blue,3)

img=np.ones((600 ,  800 ,  3) , np.uint8 )*100
 #creamos una imagen de 600x800 con 3 canales 
#Creamos una una ventana y capturamos los eventos del mouse en esa ventana
cv2.namedWindow('imagen') #--> crea una ventana de nombre imagen
cv2.setMouseCallback ('imagen',draw_circle ) #-->asociame la funcion de atenderme las acciones.
while(1) :
    #usamos la ventana creada para mostrar la imagen
    cv2.imshow('imagen', img )
    if cv2.waitKey (20) & 0xFF == 27:
        break
cv2.destroyAllWindows()
