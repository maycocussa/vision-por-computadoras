import numpy as np #importo biblioteca y le cambio de nombre por comodidad
import cv2   #a la cv2 la cambio por cv


blue= (255,0,0)
green =(0,255,0)
red=(0,0,255)

#creo una imagen rgb de color negro
img=np.zeros((512,512,3),np.uint8)
'''----------------------LINEA-------------------'''
cv2.line(img,(100,100),(200,100),blue,5)
cv2.imshow("frame",img)
key=cv2.waitKey(0)
'''----------------------RECTANGULO-------------------'''
cv2.rectangle(img,(284,50),(410,178),green,1)
cv2.imshow("frame",img)
key=cv2.waitKey(0)
'''----------------------CIRCULO-------------------'''
cv2.circle(img,(347,113),64,red,-1)
cv2.imshow("frame",img)
key=cv2.waitKey(0)
'''----------------------ELIPSE-------------------'''
cv2.ellipse(img,(256,256),(100,150),0,0,180,red,3)
cv2.imshow("frame",img)
key=cv2.waitKey(0)
'''----------------------POLIGONO-------------------'''
pts=np.array([[90,45],[215,5],[225,55],[205,35]],np.int32)
pts=pts.reshape((-1,1,2))
cv2.polylines(img,[pts],True,(0,240,250),2)
cv2.imshow("frame",img)
key=cv2.waitKey(0)
'''----------------------texto-------------------'''
font=cv2.FONT_HERSHEY_SIMPLEX
cv2.putText(img,"Mayco",(70,480),font,4,(255,255,255),2,cv2.LINE_AA)

cv2.imshow("frame",img)
key=cv2.waitKey(0)
cv2.destroyAllWindows()