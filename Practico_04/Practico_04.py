import cv2
import numpy as np
from numpy.core.fromnumeric import shape
blue = (255 ,  0 ,  0 )
green = (0 ,  255 ,  0 )
red = (0 ,  0 ,  255)

drawing =False# true si el botón está presionado

def guarda( event,x,y,flags,param ):
    global xi,yi,xf,yf ,drawing ,img #es para que no cree otra variable dentro de la funcion, y utilice la misma del main
    if event == cv2.EVENT_LBUTTONDOWN:
        drawing =True
        xi,yi =x,y
    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing is True:
            img=act.copy()  #cargo imagen sin afectar
            cv2.rectangle (img,(xi,yi),(x,y), blue, 2)        
    elif event == cv2 .EVENT_LBUTTONUP:
        drawing =False
        if x < 0: x = 0 
        if y < 0: y = 0
        xf, yf = x, y  

img=cv2.imread("hoja.jpg",1)
xi,yi=0,0
xf,yf=img.shape[0],img.shape[1]
act=img.copy()
baup=img.copy()
cv2.namedWindow(' image ')
cv2.setMouseCallback (' image ',guarda) #funciona con el while en paralelo

while 1:
    cv2.imshow(' image ',img)
    o=cv2.waitKey ( 1 ) & 0xFF
    if o ==ord('r') :
        img=baup.copy()
        act  =baup.copy()
        xi,yi=0,0
        xf,yf=img.shape[0],img.shape[1]

    
    elif o == ord("g"):
        xi,xf=min(xi,xf),max(xi,xf)
        yi,yf=min(yi,yf),max(yi,yf)
        act=baup[yi:yf, xi:xf]
        img=act.copy()
        cv2.imwrite("recorte.jpg",act)

    elif o == ord("q"):
        break

cv2.destroyAllWindows()    