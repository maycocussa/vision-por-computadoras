import cv2
import numpy as np

selec_points=[]    #puntos seleccionados

def Transform_affine(img, srcTri, dstTri):  #paso imagen de hoja,
    (h, w) = img.shape[:2] #dimension de la imagen
    warp_mat = cv2.getAffineTransform(srcTri, dstTri)
    Taff= cv2.warpAffine(img,warp_mat,(w, h),borderValue=(255, 255, 255))  
    return Taff


def mouse(event, x, y, flags, param):
    global selec_points, imgs
    if event == cv2.EVENT_LBUTTONDOWN:
        selec_points.append([x, y])
        cv2.circle(imgs, (x, y), 4, (0, 0, 255), -1)

def sel_points(img, pointnum):
    global selec_points
    selec_points = []
    cv2.namedWindow('Seleccione 3 Puntos')
    cv2.setMouseCallback('Seleccione 3 Puntos', mouse)

    while True:
        cv2.imshow('Seleccione 3 Puntos',img)
        k = cv2.waitKey(1) 
        if len(selec_points) == pointnum:
            break
    cv2.destroyAllWindows()
    print (selec_points)
    return np.array(selec_points, dtype=np.float32)

img = cv2.imread('logoutn.jpeg',1)
img_dst = cv2.imread('Facu.jpeg',1)
img = cv2.resize(img, img_dst.shape[1::-1])

baup_src = img.copy()  #hojas
baup_dst = img_dst.copy() # cartel
(h, w) = img.shape[:2]


while (True):
    cv2.imshow("Transform affine", img)
    option = cv2.waitKey(1) & 0xFF 

    if option == ord("a"):
        cv2.destroyAllWindows()
        
        imgs = baup_src.copy()  
        srcTri = sel_points(imgs,3)  #función que le mando imagen del cartel y tres puntos y devuelve un arreglo con los 3 puntos 
        
#-------------------PARA SELECCIONAR

        #mgs= img_dst.copy()  # imagen con dimención y lo guardo a imgs
       # dstTri = sel_points(imgs,3)   #le mando la imagen ,y funcion me devuelve el arreglo con los 3 puntos
#-------------------hARCODEADO        
        dstTri=np.array([[146, 237], [145, 272], [673, 253]], dtype=np.float32)
        



        img = baup_src.copy()   # copio imagen de hojas
        img_dst = baup_dst.copy()  #copio imagen de cartel en img_dst
        
        img = Transform_affine(img, srcTri, dstTri) #hojas,puntos cartel , puntos de hojas

        imgris = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        
        #genero mascara
        ret, mascara= cv2.threshold(imgris, 200, 255, cv2.THRESH_BINARY )
        #imgris --> la imagen leida en escala de grises
        #200 --> parametro de humbral
        #255 --> parametro de valor maximo
        #cv2.THRESH_BINARY --> tipo de humbralizado elegido
        
        #cv2.imshow('Resultado mascara en B&N',mascara)#resultado de la mascara en blanco y negro
        mascara_inv = cv2.bitwise_not(mascara) #invierto la mascara
        #cv2.imshow('Imagen destino',img_dst)
        #cv2.imshow('Mascara invertida',mascara_inv)
        #cv2.imshow('Mascara',mascara)
        img1_dst = cv2.bitwise_and(img_dst,img_dst, mask=mascara)  #paso dos imagenes de cartel y la mascara
        img_2 = cv2.bitwise_and(img,img, mask=mascara_inv)  #ojo mascara inversa
        #cv2.imshow('A',img_2)  #todo negro con la src en color
        img = cv2.add(img1_dst,img_2) #realizo la suma de las dos imagenes

    elif option == ord('g'):
        cv2.imwrite('Trasformacion_Affine.jpg', img)

    elif option == ord('q'):
        break