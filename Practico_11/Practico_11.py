import cv2
import numpy as np


MIN_MATCH_COUNT = 10

img1 = cv2.imread('Foto1.jpg')     #Leemos imagen 1
img2 = cv2.imread('Foto2.jpg')     #Leemos imagen 2
img1_backup= img1.copy()
img2_backup = img2.copy()

dscr = cv2.SIFT_create(100)   #Inicialización del detector y el descriptor
print ("Debug") 
kp1, des1 = dscr.detectAndCompute(img1, None) #Encontramos los puntos clave y los descriptores con SIFT en img1
print ("Debug") 
kp2, des2 = dscr.detectAndCompute(img2, None) #Encontramos los puntos clave y los descriptores con SIFT en img2
print ("Debug") 
# Gráfica de puntos clave
cv2.drawKeypoints(img1,kp1,img1_backup,(0,255,0))
cv2.drawKeypoints(img2,kp2,img2_backup,(100,255,0))
concat_img = np.concatenate((img1_backup, img2_backup), axis=0) #Concatenamos verticalmente
cv2.imwrite('Puntos_de_interes.jpg',concat_img)
cv2.imshow('Puntos de Interes', concat_img)
cv2.waitKey(0)

# Establecemos matches
matcher = cv2.BFMatcher(cv2.NORM_L2)
matches = matcher.knnMatch(des1, des2, k=2)
print ("Debug") 
# Guardamos los buenos matches usando el test de razón de Lowe
good = []
all = []
for m, n in matches:
  all.append(m)
  if m.distance < 0.7*n.distance:
    good.append(m)
print ("Debug") 
if(len(good)>MIN_MATCH_COUNT):
  scr_pts = np.float32([kp1[m.queryIdx].pt for m in good]).reshape(-1, 1, 2)
  dst_pts = np.float32([kp2[m.trainIdx].pt for m in good]).reshape(-1, 1, 2)
  H,mask = cv2.findHomography(dst_pts, scr_pts, cv2.RANSAC, 5.0)  #Computamos la homografía con RANSAC

wimg2 = cv2.warpPerspective(img2, H, img2.shape[:2][::-1])   #Aplicamos la transformación perspectiva H a img2

# Gráfica de matches pre Lowe
img_match = cv2.drawMatches(img1, kp1, img2, kp2, all, None) 
cv2.imwrite('Pre lowe.jpg',img_match)
cv2.imshow('match pre-Lowe', img_match)
cv2.waitKey(0)
print ("Debug") 
# Gráfica de matches post Lowe
img_match = cv2.drawMatches(img1, kp1, img2, kp2, good, None) 
cv2.imwrite('Post_Lowe.jpg',img_match)
cv2.imshow('match post Lowe', img_match)
cv2.waitKey(0)

#Mezclamos ambas imágenes
alpha = 0.5
blend = np.array( wimg2*alpha + img1*(1-alpha), dtype=np.uint8)
cv2.imshow('Imagen Mezclada', blend)
cv2.imwrite('imagen_mezclada.jpg',blend)
cv2.waitKey(0)
cv2.destroyAllWindows