
import math
import cv2
import numpy as np
from numpy.core.fromnumeric import shape

blue = (255 ,  0 ,  0 )
green = (0 ,  255 ,  0 )
red = (0 ,  0 ,  255)

drawing =False# true si el botón está presionado
patron=0.04926108374    # proporcion de medicion con respecto a la medida en cm/pixeles
                        #(203 pixeles(de la base ) / 10 cm   = 20.3       
                        # 20.3*x = 1 cm    x= 0.04926108374 )
xi,yi=0,0

def perspectiva(imagen, srcPts, dstPts):
    (h, w) = imagen.shape[:2]
    Pers = cv2.getPerspectiveTransform(srcPts, dstPts)
    rect = cv2.warpPerspective(img, Pers, (w, h), borderMode=cv2.BORDER_CONSTANT)
    return rect

def medir( event,x,y,flags,param ):
    global xi,yi,drawing ,img_r ,img_r_med #es para que no cree otra variable dentro de la funcion, y utilice la misma del main
    if event == cv2.EVENT_LBUTTONDOWN:
        drawing =True
        xi,yi =x,y
    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing is True:
            img_r= img_r_med.copy()
            cv2.line(img_r,(xi,yi),(x,y),green,2)
            distancia=np.sqrt((xi-x)**2+(yi-y)**2)
            distancia=distancia*(0.7968127)
            text= int(xi), int(yi)
            cv2.putText(img_r, "{:.2f} cm" .format(distancia),text,cv2.FONT_HERSHEY_PLAIN,1,red,2)
    elif event == cv2 .EVENT_LBUTTONUP:
        drawing =False
        img_r_med=img_r.copy()

img = cv2.imread('photo.jpg', cv2.IMREAD_COLOR)

dst_Puntos = np.array([[467, 433], [578, 433], [578, 683], [467, 683]], dtype=np.float32) #  dstPts (ptos destinos ) harcodeo para que esten en la misma linea y que sean equidistante  
src_Puntos = np.array([[467,  433], [576,426], [589, 679],[468, 677]], dtype=np.float32)     # harcodeo los selected_point

img_r = perspectiva(img, src_Puntos, dst_Puntos)  
img_r_c = img_r.copy()
img_r_med= img_r.copy()


cv2.namedWindow('Rectificada,Medir')
cv2.setMouseCallback ('Rectificada,Medir', medir) #funciona con el while en paralelo

while True:
    cv2.imshow('Rectificada,Medir',img_r)
    
    option = cv2.waitKey(1) & 0xFF 
    
    if option == ord('r'): 

        img_r=img_r_c.copy()
        img_r_med=img_r.copy()
    
    elif option == ord('g'):
        cv2.imwrite('Resultado_guardado.png', img_r)  #escribo la imgaen como rectificado.png

    elif option == ord('q'):
        break

cv2.destroyAllWindows()    