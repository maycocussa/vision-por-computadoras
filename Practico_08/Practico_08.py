import cv2
import numpy as np


selec_points=[]


def perspectiva(imagen, srcPts, dstPts):
    (h, w) = imagen.shape[:2]
    Pers = cv2.getPerspectiveTransform(srcPts, dstPts)
    rect= cv2.warpPerspective(img, Pers, (w, h))
    return rect 


def mouse(event, x, y, flags, param):
    global selec_points, imgs
    if event == cv2.EVENT_LBUTTONDOWN:
        selec_points.append([x, y])
        cv2.circle(imgs, (x, y), 4, (0, 0, 255), -1)


def sel_points(imag, pointnum):
    global selec_points
    selec_points = []
    cv2.namedWindow('Seleccione 4 Puntos')
    cv2.setMouseCallback('Seleccione 4 Puntos', mouse)

    while True:
        cv2.imshow('Seleccione 4 Puntos', imag)
        k = cv2.waitKey(1)
        if len(selec_points) == pointnum:
            break
    cv2.destroyAllWindows()

    return np.array(selec_points, dtype=np.float32)


img = cv2.imread('hoja.jpg', cv2.IMREAD_COLOR)
img = cv2.resize(img, (800, 600)) #acomodo el tamaño
backup = img.copy()
(h, w) = img.shape[:2]
while (True):
    cv2.imshow('Perspectiva', img)
    option = cv2.waitKey(1) & 0xFF

    if option == ord('h'):
        cv2.destroyAllWindows()
        imgs = backup.copy()
        img = backup.copy()
        src_puntos = sel_points(imgs, 4)
        #print (src_puntos)
        dst_puntos = np.array([[0, 0], [w, 0], [w, h], [0, h]], dtype=np.float32)
        img = perspectiva(img, src_puntos ,dst_puntos)

    elif option == ord('g'):
        cv2.imwrite('rectificado.png', img)

    elif option == ord('q'):
        break