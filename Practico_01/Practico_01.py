import random

def adivinar(intentos):
    numero = random.randint(0,100)
    running= True
    contador=0
    while running:
        intentos=intentos-1
        contador=contador+1
        numi= int(input("Ingrese el Numero entre 0 y 100: "))
        
        if numi == numero:
            print("Adivinaste el Numero con: "+ str(contador)+" intentos")
            running=False
        elif numi<numero:
            print("El numero ingresado es menor")
        else:
            print("El numero ingresado es mayor")
        
        if(intentos==0):
            print("Perdiste los "+ str(contador) +" intentos")
            running=False


numeral= int(input("Ingrese el Numero de intentos deseados: "))
adivinar(numeral)