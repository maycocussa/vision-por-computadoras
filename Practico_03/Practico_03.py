import cv2
import sys

##if(len(sys.argv)>1):     
   ## filename = sys.argv[1]
##else:
  ##  print("Pasar el nombre del video como argumento 1")
   ## sys.exit(0)

videoCapture= cv2.VideoCapture("video.mp4") #creamos objeto de video captura

fourcc_MP4V = cv2.VideoWriter_fourcc('M','P','4','V') #formato .mp4 -> creo codigo de 4 char
#fourcc_X264 = cv2.VideoWriter_fourcc('X','2','6','4') #formato .mp4
#fourcc_THEO = cv2.VideoWriter_fourcc('T','H','E','O') #formato .ogv
#fourcc_XVID = cv2.VideoWriter_fourcc('X','V','I','D')  #formato .avi
#fourcc_I420 = cv2.VideoWriter_fourcc('I','4','2','0') #formato .avi
#fourcc_PIM1 = cv2.VideoWriter_fourcc('P','I','M','1') f#ormato .avi
#fourcc_FLV1 = cv2.VideoWriter_fourcc('F','L','V','1') #formato .flv
#(framesize)= frameshape[:2] -->forma de obtener tamaño

Fps=videoCapture.get(cv2.CAP_PROP_FPS)  #con este codigo obtenemos fps de video no harcodeado

Fsize = (int(videoCapture.get(cv2.CAP_PROP_FRAME_WIDTH)) , int(videoCapture.get(cv2.CAP_PROP_FRAME_HEIGHT))) 
#con el codigo de arriba obtengo alto y ancho , lo paso a entero y guardo en una tupla


delay= int(1000/(Fps)) #divido 1000ms en la cantidad de frames

read, frame = videoCapture.read()  #leo el cuadro y read me devuelvo true o false si leyo o no

videowriter=cv2.VideoWriter('resultado.mp4',fourcc_MP4V,Fps,Fsize)

#mientras read sea true continua el while
while read:
    gray=cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
    videowriter.write(gray) #escribo el frame ya pasado en gris
    cv2.imshow("Video modificado",gray) #muestro frame por pantalla
    if(cv2.waitKey(delay) & 0xFF == ord("q")):   #espero el delay preguntando si apreto la q
        break
    read, frame = videoCapture.read()  #si no apreto q leemos el siguiente cuadro


videoCapture.release()
videowriter.release()
cv2.destroyAllWindows()

